package com.pinyougou.page.service.impl;

import com.pinyougou.page.service.ItemPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 * @ClassName : PageListener
 * @Description :
 * @Auther : zhangyan
 * @Date :  2018/12/31 13:28
 */
@Component
public class PageListener implements MessageListener {
    @Autowired
    private ItemPageService itemPageService;
    @Override
    public void onMessage(Message message) {
        ObjectMessage objectMessage=(ObjectMessage)message;
        try {
            Long[]  goodsIds = (Long[]) objectMessage.getObject();
            for (Long id : goodsIds){
                itemPageService.genItemHtml(id);
            }
            System.out.println("生成静态页面"+goodsIds);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
