package com.pinyougou.cart.service;

import com.pinyougou.pojo.Cart;

import java.util.List;

/**
 * @ClassName : CartService
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/01/07 21:55
 */
public interface CartService {
    public List<Cart> addGoodsToCartList(List<Cart> cartList,Long sellerId,Integer num) throws Exception;

    /**
     * 从redis中查询购物车
     * @param username
     * @return
     */
    public List<Cart> findCartListFromRedis(String username);

    /**
     * 将购物车保存到redis
     * @param username
     * @param cartList
     */
    public void saveCartListToRedis(String username,List<Cart> cartList);
    /**
     * 合并购物车
     * @param cartList1
     * @param cartList2
     * @return
     */
    public List<Cart> mergeCartList(List<Cart> cartList1,List<Cart> cartList2);


}
