package com.pinyougou.manager.controller;

import entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import utils.UuidUtils;

import java.time.LocalDate;

@RestController
public class UploadController {
    @Value("${domain}")
    private String domain;
    @Autowired
    private OssManager ossManager;

    /**
     *      * https://pinyougou-zhangxu.oss-cn-hangzhou.aliyuncs.com/file/20181213013805b77ca34c5494b1151c47930bfb.jpg
     * @param file
     * @return
     */
    @RequestMapping("/ossUpload")
   public Result upload (MultipartFile file){
        if(file == null || file.getSize() == 0){
            return new Result(false,"图片为空");
        }
        String filename = file.getOriginalFilename();
        String suffix = filename.substring(filename.lastIndexOf("."));
        if(!checkImg(suffix)){
            return new Result(false,"图片格式不正确");
        }
        String key = "file/" + LocalDate.now().toString().replaceAll("-", "") + "/"
                + UuidUtils.getUUID() + "/" + filename;
        boolean flag = ossManager.uploadFile(file, key, suffix);
        if(!flag){
            return new Result(false,"上传失败");
        }
        String url = domain+"/"+key;
        return  new Result(true,url);
   }

   private boolean checkImg(String suffix){
        String [] suff = {"png","jpg","bmp","jpeg"};
        suffix = suffix.toLowerCase();
        for (String suf : suff){
            if(suffix.endsWith(suf)){
                return true;
            }
        }
        return false;
   }

}

