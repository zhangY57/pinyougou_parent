package com.pinyougou.page.service;

/**
 * @ClassName : ItemPageService
 * @Description :
 * @Auther : zhangyan
 * @Date :  2018/12/26 22:22
 */
public interface ItemPageService {

    /**
     * 生成商品详细页
     * @param goodsId
     */
     boolean genItemHtml(Long goodsId);

    public boolean deleteItemHtml(Long[] goodsIds);


}
