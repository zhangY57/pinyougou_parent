package utils;

import java.util.UUID;

public class UuidUtils {

    public static String getUUID(){
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replaceAll("-", "");
    }
}
