package com.pinyougou.shop.controller;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.HttpMethod;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.GeneratePresignedUrlRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import utils.FastDFSClient;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;

/**
 * 文件上传管理类
 */
@Component
public class OssManager {
    private FastDFSClient fastDFSClient;
    @Value("${FILE_SERVER_URL}")
    private String file_server_url;
    @Value("${endpoint}")
    private String endpoint;
    @Value("${bucketName}")
    private String bucketName;
    @Value("${accessKeyId}")
    private String accessKeyId;
    @Value("${accessKeySecret}")
    private String accessKeySecret;
    private Logger logger = LoggerFactory.getLogger(UploadController.class);

    /**
     * 文件上传
     * @param file
     * @return
     */
    public boolean uploadFile(MultipartFile file,String key,String suffix){
        if(!checkImg(suffix)){
            return false;
        }
        OSSClient ossClient = null;
        try {
            ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
            ossClient.putObject(bucketName, key, file.getInputStream());
            ossClient.shutdown();
            return true;
        } catch (OSSException oe) {
            logger.error("oss upload error", oe);
            return false;
        } catch (ClientException ce) {
            logger.error("oss upload error", ce);
            return false;
        } catch (FileNotFoundException fe) {
            logger.error("oss upload error", fe);
            return false;
        } catch (IOException ioe) {
            logger.error("oss upload error", ioe);
            return false;
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }

    /**
     * 判断图片格式
     * @param suffix
     * @return
     */
    private boolean checkImg(String suffix) {
        String[] imgSuffix = { "png", "jpg", "jpeg", "gif", "bmp" };
        for (String suf : imgSuffix) {
            suffix = suffix.toLowerCase();
            if (suffix.endsWith(suf)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取文件url路径
     * @param key
     * @return
     */
    public String getUrl(String key) {
        String urlStr = null;
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        try {
            // 过期时间30分钟
            Date expiration = new Date(System.currentTimeMillis() + 1000 * 60 * 30);
            GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(bucketName, key,
                    HttpMethod.GET);
            req.setExpiration(expiration);
            URL signedUrl = ossClient.generatePresignedUrl(req);
            if (signedUrl != null) {
                urlStr = signedUrl.toURI().toString();
            }
        } catch (OSSException oe) {
            logger.error("oss upload error", oe);
            return null;
        } catch (ClientException ce) {
            logger.error("oss upload error", ce);
        } catch (URISyntaxException ue) {
            logger.error("oss upload error", ue);
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return urlStr;
    }
}
