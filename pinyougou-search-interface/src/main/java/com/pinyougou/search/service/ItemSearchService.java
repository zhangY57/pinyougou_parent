package com.pinyougou.search.service;

import com.pinyougou.pojo.TbItem;

import java.util.List;
import java.util.Map;

public interface ItemSearchService {

    Map<String,Object> search(Map searchMap);

    /**
     * 批量导入
     * @param items
     */
    void importList(List<TbItem> items);

    /**
     * 批量删除
     * @param ids
     */
    void deleteByGoodsIds(Long[] ids);
}
